/**Task 1**: Right now, the function fetchRandomNumbers can be used by passing a callback,
Your task is to promisfy this function so that the following can be done:

fetchRandomNumbers().then((randomNum) => {
    console.log(randomNum)
});

Similarlly, do the same for the function fetchRandomString*/

function fetchRandomNumbers() {
    return new Promise((resolve, reject) => {
        console.log('Fetching number...');
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString() {
    return new Promise((resolve, reject) => {
        console.log('Fetching string...');
        setTimeout(() => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 5; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

/**Task 2**: Fetch a random number -> add it to a sum variable and print sum-> fetch another random variable
-> add it to the same sum variable and print the sum variable.*/

function task2(){
    let sum = 0;
    function print(num) {
            sum += num;
            console.log(sum);
    }

    fetchRandomNumbers()
        .then(print)
        .then(fetchRandomNumbers())
        .then(print)
        .catch(err => console.log(err))
}

/**Task 3**: Fetch a random number and a random string simultaneously, concatenate their
and print the concatenated string*/

function task3(){
    Promise.all([fetchRandomNumbers(), fetchRandomString()]).then((values) => {
        console.log(values[0]+values[1])
    }).catch(err => console.log(err))
}   

/**Task 4**: Fetch 10 random numbers simultaneously -> and print their sum.*/

function task4(n){
    let sum = 0;
    let promiseArr = [];
    for(let i=1; i<=n; i++){
        promiseArr.push(fetchRandomNumbers());      
    }
    Promise.all(promiseArr).then(values => {
        for(let i=0; i<values.length; i++){
            sum += values[i];
        }
        console.log(sum);
    }).catch(err => console.log(err))
}

